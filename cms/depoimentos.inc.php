<script type="text/javascript">
$(function() {
    $("#data").datepicker({
		dateFormat: 'dd/mm/yy',
		dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
		dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
		dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
		nextText: 'Próximo',
		prevText: 'Anterior'
	});
});
</script>
<section>
<h1 class="tit-secao">Depoimento</h1>

<?php ShowErros(); ?>

<form action="action_depoimentos.php?do=CadastraDepoimento" method="post" enctype="multipart/form-data">

<div class="form-group">
    <label class="control-label col-sm-2" for="titulo">Nome:</label>
    <div class="col-sm-10">
    <input name="titulo" type="text" class="form-control grande" id="titulo" /><br />
	</div>
</div>

<div class="form-group">
    <label class="control-label col-sm-2" for="empresa">Empresa:</label>
    <div class="col-sm-10">
    <input name="empresa" type="text" class="form-control medio" id="empresa" /><br />
	</div>
</div>

<div class="form-group">
    <label class="control-label col-sm-2" for="cargo">Cargo:</label>
    <div class="col-sm-10">
    <input name="cargo" type="text" class="form-control medio" id="cargo" /><br />
	</div>
</div>

<div class="form-group">
    <label class="control-label col-sm-2">Texto:</label>
    <div class="col-sm-10">
    <textarea class="form-control" name="desc" id="desc"></textarea><br />
	</div>
</div>
<div class="form-group">
    <label class="control-label col-sm-2" for="data">Publicar dia:</label>
    <div class="col-sm-10">
    <input name="data" type="data" class="form-control pequeno" id="data" value="<?=date('d/m/Y')?>" /><br />
    <p class="info" style="padding:5px; background-color:#FFFF99; border:1px dashed #256574; color:#256574;">Para agendar uma publicação, informe a partir de qual data ela deve ser exibida no site, exemplo <b><?=mostraData(proximoDia($dias = 4))?></b></p>
	</div>
</div>
<div class="form-group">
	<button type="submit" class="btn">CADASTRAR <i class="fa fa-check" aria-hidden="true"></i></button>
</div>
</form>
</section>

  <?php
$res = mysql_query("SELECT * FROM site_tb_depoimentos ORDER BY data_dep DESC, id_dep DESC");
if (mysql_num_rows($res)) {
?>
<section class="lista-registros">
<h1>Depoimentos Cadastrados</h1>

<p>Clique para editar</p>
<p> Legenda: <span style="background: #FFFF99; padding: 3px;">Depoimento agendado.</span></p>

<table class="table table-striped table-datatables">
	<thead>
	<tr>
    	<th>Data</th>
   		<th>Nome</th>
        <th>Empresa</th>
        <th>Cargo</th>
        <th class="tbl_acao">Excluir</th>
  	</tr>
    </thead>
    <tbody>
    <?php while ($row = mysql_fetch_array($res)) { 
	$hoje = date("Y-m-d");
	if($row['data_dep'] > $hoje){ $bg = 'style="background-color: rgb(255, 255, 153)"'; } else { $bg = '"';}
	?>
<tr>
      <td <?=$bg?> align="center"><a href="./?p=depoimento&id_dep=<?=$row['id_dep']?>"><?=mostraData($row['data_dep'])?></a></td>
      <td <?=$bg?> align="center"><a href="./?p=depoimento&id_dep=<?=$row['id_dep']?>"><?=mostraChar($row['nome_dep'])?></a></td>
      <td <?=$bg?> align="center"><a href="./?p=depoimento&id_dep=<?=$row['id_dep']?>"><?=mostraChar($row['empresa_dep'])?></a></td>
      <td <?=$bg?> align="center"><a href="./?p=depoimento&id_dep=<?=$row['id_dep']?>"><?=mostraChar($row['cargo_dep'])?></a></td>
      <td <?=$bg?> align="center" class="excluir"><a onclick="return Confirma('Deseja excluir o depoimento de <?=mostraChar($row['nome_dep'])?>?')" href="action_depoimentos.php?do=ExcluiDepoimento&id_dep=<?=$row['id_dep']?>"><i class="fa fa-times btn_excluir" aria-hidden="true"></i></a></td>
  </tr>
    <?php } ?>
    </tbody>
</table>
</section>
<?php
}
?>