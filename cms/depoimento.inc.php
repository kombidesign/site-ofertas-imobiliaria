<script type="text/javascript">
$(function() {
    $("#data").datepicker({
		dateFormat: 'dd/mm/yy',
		dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
		dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
		dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
		nextText: 'Próximo',
		prevText: 'Anterior'
	});
});
</script>
<?php
if (is_numeric($_GET['id_dep'])) {

	$id_dep = $_GET['id_dep'];
	$res = mysql_query("SELECT * FROM site_tb_depoimentos WHERE id_dep = $id_dep");
	if (mysql_num_rows($res)) {
		$row = mysql_fetch_array($res);

	} else {
		Redir('./?p=depoimentos');
	}
} else {
	Redir('./?p=depoimentos');
}
?>
<section>
<h1>Depoimento - Detalhes</h1>

<p>Modifique os campos a seguir e pressione "SALVAR" para alterar os dados.</p>

<?php ShowErros(); ?>

<form action="action_depoimentos.php?do=AlteraDepoimento&id_dep=<?=$row['id_dep']?>" method="post" enctype="multipart/form-data">

<div class="form-group">
    <label class="control-label col-sm-2" for="titulo">Nome:</label>
    <div class="col-sm-10">
    <input name="titulo" type="text" class="form-control grande" id="titulo" value="<?=mostraChar($row['nome_dep'])?>" /><br />
	</div>
</div>

<div class="form-group">
    <label class="control-label col-sm-2" for="empresa">Empresa:</label>
    <div class="col-sm-10">
    <input name="empresa" type="text" class="form-control medio" id="empresa" value="<?=mostraChar($row['empresa_dep'])?>" /><br />
	</div>
</div>

<div class="form-group">
    <label class="control-label col-sm-2" for="cargo">Cargo:</label>
    <div class="col-sm-10">
    <input name="cargo" type="text" class="form-control medio" id="cargo" value="<?=mostraChar($row['cargo_dep'])?>" /><br />
	</div>
</div>

<div class="form-group">
    <label class="control-label col-sm-2">Texto:</label>
    <div class="col-sm-10">
    <textarea class="form-control" name="desc" id="desc"><?=mostraChar($row['desc_dep'])?></textarea><br />
	</div>
</div>
<div class="form-group">
    <label class="control-label col-sm-2" for="data">Publicar dia:</label>
    <div class="col-sm-10">
    <input name="data" type="data" class="form-control pequeno" id="data" value="<?=mostraData($row['data_dep'])?>" /><br />
    <p class="info" style="padding:5px; background-color:#FFFF99; border:1px dashed #256574; color:#256574;">Para agendar uma publicação, informe a partir de qual data ela deve ser exibida no site, exemplo <b><?=mostraData(proximoDia($dias = 4))?></b></p>
	</div>
</div>

<div class="form-group">
	<button type="submit" class="btn">SALVAR <i class="fa fa-check" aria-hidden="true"></i></button>
    <a href="index.php?p=depoimentos" class="btn btn-gray pull-right">Voltar <i class="fa fa-chevron-left" aria-hidden="true"></i></a>
</div>
</form>
</section>