<?php
session_start();
include 'cms/config/config.php';
require 'cms/classes/class.conndatabase.php';
require 'cms/classes/functions.php';
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-br" itemscope itemtype="http://schema.org/WebPage"> <!--<![endif]-->
<head>
    <? include('includes/metas.php');?>
    
    <? include('includes/css.php');?>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
</head>

<body>

<? include('includes/header.php');?>

<div class="page"> <!--Elemento de formatação-->

<div class="container">
    <div class="row">
		<div class="col-md-12">
            <h1>Depoimentos</h1>
        </div>


		<div class="col-md-12">

            <div class="depoimento-clientes">

                <?php
                $resDep = mysql_query("SELECT * FROM site_tb_depoimentos ORDER BY data_dep DESC");
                if (mysql_num_rows($resDep)) {

                while ($rowDep = mysql_fetch_array($resDep)) {
                ?>
                    <div class="slider-cli">
                        <div class="depoimento-box">
                            <blockquote>
                                <p><?=$rowDep['desc_dep']?></p>

                            </blockquote>
                            <p class="autor">
                                <?=$rowDep['nome_dep']?>
                                <? if($rowDep['empresa_dep']!=""){ echo " - ".$rowDep['empresa_dep']; }?>
                                <? if($rowDep['cargo_dep']!=""){ echo " - ".$rowDep['cargo_dep']; }?></p>
                        </div>
                    </div>
                <? } ?>

                <? } ?>

            </div>

        </div>

    </div>
    
  </div>
</div>


</div> <!--Fim do elemento de formatação-->

<? include('includes/footer.php');?>

<? include('includes/js.php');?>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.slick/1.5.7/slick.min.js"></script>

<script>
$(document).ready(function(){
	$('.depoimento-clientes').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		adaptiveHeight: true,
		autoplaySpeed: 2000,
		dots:true,
	});
});
</script>
<? include('includes/analytics.php');?>

</body>
</html>